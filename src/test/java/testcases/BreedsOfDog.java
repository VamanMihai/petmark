package testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import base.TestBase;

public class BreedsOfDog extends TestBase {

	@Test
	public void BreedsOfDog() {

		System.setProperty("org.uncommons.reportng.escape-output", "false"); //This is a flag for ignore html code in html page
		driver.findElement(By.cssSelector(objectRepo.getProperty("breedsOfDogSection"))).click();

		log.debug("Inside BreedsOfDog Test");

		driver.findElement(By.cssSelector(objectRepo.getProperty("Affenpinscher"))).click();
        
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("img[alt='08-Affenpinscher.jpg']")));
		Assert.assertTrue(driver.getCurrentUrl().contains("Affenpinscher"), "Click dosen't work :(");
//		Assert.assertTrue(isElementPresent(By.cssSelector(objectRepo.getProperty("dogs tab"))));		
		log.debug("Page Affenpinscher is here!!!");
		Reporter.log("Page Affenpinscher is here!!!");
		Reporter.log("<a target='_blank' <a href='C:\\Users\\mihai.vaman\\Desktop\\alert pop-up.JPG'>Screenshot</a>");//target blank is for open the screenshot in a new tab
		Reporter.log("<br>");
		Reporter.log("<a target='_blank' <a href='C:\\Users\\mihai.vaman\\Desktop\\alert pop-up.JPG'><img src='C:\\Users\\mihai.vaman\\Desktop\\alert pop-up.JPG' height=200 width=600></img></a>");

	}

}
