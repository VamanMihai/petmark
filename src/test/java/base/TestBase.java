package base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class TestBase {

	/*
	 * WebDriver for mac or windows -> firefox, chrome, IE, Safary Properties -
	 * config file Logs ExtendReports - test-output -> index.html or
	 * emailable-report.html DB Excel Mail ReportNG, ExtendReports Jenkins
	 * 
	 */

	public static WebDriver driver;
	public static Properties config = new Properties();
	public static Properties objectRepo = new Properties();
	public static FileInputStream fis;
	public static Logger log = Logger.getLogger("devpinoyLogger");
	public static WebDriverWait wait;

//	public static ExcelReader excel = new ExcelReader(System.getProperty("user.dir") + "\\src\\test\\resources\\excel\\testdata.xlsx");

	@BeforeSuite
	public void setUp() {

		if (driver == null) {

			try {
				fis = new FileInputStream(
						System.getProperty("user.dir") + "//src//test//resources//properties//Config.properties");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				config.load(fis);
				log.debug("Config file loaded!!!");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				fis = new FileInputStream(
						System.getProperty("user.dir") + "//src//test//resources//properties//ObjectRepo.properties");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				objectRepo.load(fis);
				log.debug("ObjectRepo file loaded!!!");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

//			//For MAC
//			if (config.getProperty("browser").equals("firefox")) {
//
//				System.setProperty("webdriver.gecko.driver",
//						System.getProperty("user.dir") + "//src//test//resources//executables//geckodriver");
//				driver = new FirefoxDriver();
//
//			} else if (config.getProperty("browser").equals("chrome")) {
//
//				// For Windows we need to use "\\"
//				System.setProperty("webdriver.chrome.driver",
//						System.getProperty("user.dir") + "//src//test//resources//executables//chromedriver");
//				driver = new ChromeDriver();
//
//			} else if (config.getProperty("browser").equals("ie")) {
//
//				System.setProperty("webdriver.ie.driver",
//						System.getProperty("user.dir") + "//src//test//resources//executables//IEDriverServer.exe");
//				driver = new InternetExplorerDriver();
//
//			}

			// For Windows
			if (config.getProperty("browser").equals("firefox")) {

				// System.setProperty("webdriver.gecko.driver", "gecko.exe");
				driver = new FirefoxDriver();

			} else if (config.getProperty("browser").equals("chrome")) {

				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "\\src\\test\\resources\\executables\\chromedriver.exe");
				log.debug("Chrome Launched !!!");
				driver = new ChromeDriver();
			} else if (config.getProperty("browser").equals("IE")) {

				System.setProperty("webdriver.IE.driver",
						System.getProperty("user.dir") + "\\src\\test\\resources\\executables\\IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
			driver.get(config.getProperty("homePageUrl"));
			log.debug("Navigated to :" + config.getProperty("homePageUrl"));
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Integer.parseInt(config.getProperty("implicit.wait")),
					TimeUnit.SECONDS); // this is implicitWait
			wait = new WebDriverWait(driver, 5); // this is explicitWait

		}

	}

	public boolean isElementPresent(By by) {

		try {

			driver.findElement(by);
			return true;

		} catch (NoSuchElementException e) {
			return false;
		}
	}

	@AfterSuite
	public void tearDown() {

		if (driver != null) {
			driver.quit();
		}
		log.debug("Test execution completed !!!");
	}
}
