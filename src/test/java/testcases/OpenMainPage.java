package testcases;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import base.TestBase;

public class OpenMainPage extends TestBase {
	
	
	@Test
	public void openMainPage() throws InterruptedException {
		
		log.debug("Inside openMainPage Test");
		
		driver.findElement(By.cssSelector(objectRepo.getProperty("logo"))).click();

		Assert.assertTrue(driver.getCurrentUrl().contains("acasa"), "Click dosen't work :(");
//		Assert.assertTrue(isElementPresent(By.cssSelector(objectRepo.getProperty("dogs tab"))));		
		log.debug("Page refreshed");
		
	}

}
